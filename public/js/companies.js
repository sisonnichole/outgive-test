var DataTables = (function () {
    var handleTables = function () {
      $('#companies-datatable').DataTable({
        responsive: true,
        searching: true
      })
    }
  
    return {
      // main function to initiate the module
      init: function () {
        handleTables()
      }
    }
  })()
  
  jQuery(document).ready(function () {
    DataTables.init()
  })
  
  // Show delete/update modal for actions
  $(document).ready(function () {
    var companiesTable = $('#companies-datatable').DataTable();
    $('#companies-datatable').on('click', 'tr', function () {
      console.log('this')
      var company = $('td', this).eq().text();
      $("#update_company_id").val(companiesTable.row(this).data()['DT_RowId']);
      $("#update_company_name").val(companiesTable.row(this).data()[0]);
      $("#update_company_email").val(companiesTable.row(this).data()[1]);
      $("#update_company_password").val(companiesTable.row(this).data()[2]);
      $("#update_company_website").val(companiesTable.row(this).data()[3]);
      $('#companiesModal').modal("show");
    });
  
  });
  
  
  
  