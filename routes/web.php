<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\UserController::class, 'show'])->name('user.details');
Route::get('/', [App\Http\Controllers\UserController::class, 'show'])->name('user.details');


Route::get('/companies', [App\Http\Controllers\HomeController::class, 'index'])->name('companies');
Route::get('/newsletter', [App\Http\Controllers\HomeController::class, 'newsletter'])->name('newsletter');
Route::post('/add-company', [App\Http\Controllers\HomeController::class, 'store'])->name('company.store');
Route::post('/update-company', [App\Http\Controllers\HomeController::class, 'update'])->name('company.update');
Route::get('/settings', [App\Http\Controllers\SettingsController::class, 'index'])->name('settings.index');
Route::get('/update', [App\Http\Controllers\SettingsController::class, 'update'])->name('settings.update');


Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

