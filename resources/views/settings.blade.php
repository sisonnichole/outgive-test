@extends('layouts.app')
@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="card no-border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 header-text-padding">
                            <div class="col-sm-9 header-text-padding">
                                <h5>Settings</h5>
                                <div class="card-actions">
                                </div>
                            </div>
                            <div class="card-body inner-2">
                                <form action="{{route('settings.update')}}" method = "POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="modal-body">
                                        <div class="row dataTable">
                                            <div class="col-md-12">
                                                <input type="hidden" class="form-control"  id="settings_id" name="settings_id">
                                            </div>
                                        </div>

                                        <div class="row dataTable">
                                            <div class="col-md-12">
                                                <label class="control-label">Email (recipient when there's new company registered)</label>
                                            </div>
                                            <br>
                                            <div class="col-md-12">
                                                <input type="text" value="{{$settings[0]->value}}" class="form-control" id="gen_email_for_registration" name="gen_email_for_registration" required>
                                            </div>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="col-sm-12 no-padding">
                                            <button style="float: right;" class="btn btn-primary edit" value="update" name="action" type="submit"><i class="icon-fa icon-fa-save">
                                            </i>Save Changes</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
