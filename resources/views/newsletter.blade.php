@extends('layouts.app')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="card no-border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-9 header-text-padding">
                            <h5>Newsletter</h5>
                        </div>
                        <hr>
                        <div class="card-body inner-2">
                            <div class="container">
                                @foreach ($responses as $response)
                                    <div class="row">
                                        <div class="col-5">
                                            <img class="img-fluid" src="{{ $response['urlToImage'] }}" width="80%"/>
                                        </div>
                                        <div class="col-3">
                                            <div>
                                                Source:
                                                <span>{!! $response['source']['name'] !!}</span>
                                            </div>
                                            <div>
                                                Author:
                                                <span>{!! $response['author'] !!}</span>
                                            </div>
                                            <div>
                                                Date Published:
                                                <span>{!! $response['publishedAt'] !!}</span>
                                            </div>
                                            <div>
                                                Link:
                                                <span><a href="{!! $response['url'] !!}" target="_blank">{!! $response['url'] !!}</a></span>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div>
                                                Title:
                                                <span>{!! $response['title'] !!}</span>
                                            </div>
                                            <div>
                                                Description:
                                                <span>{!! $response['description'] !!}</span>
                                            </div>
                                            <div>
                                                Content:
                                                <p>{{ $response['content'] }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach
                            </div>
                            <!-- @foreach ($responses as $response)
                                <div class="row" style="float: left">
                                    <img src="{{ $response['urlToImage'] }}" width="30%"/>
                                </div>
                                <div class="row" style="float: right">
                                    <div class="col-md-6">
                                        Source:
                                        <span>{!! $response['source']['name'] !!}</span>
                                    </div>

                                    <div class="col-md-6">
                                        Author:
                                        <span>{!! $response['author'] !!}</span>
                                    </div>

                                    <div class="col-md-6">
                                        Date Published:
                                        <span>{!! $response['publishedAt'] !!}</span>
                                    </div>

                                    <hr>

                                    <div class="col-md-6">
                                        Title:
                                        <span>{!! $response['title'] !!}</span>
                                    </div>

                                    <div class="col-md-6">
                                        Description:
                                        <span>{!! $response['description'] !!}</span>
                                    </div>

                                    <div class="col-md-6">
                                        Link:
                                        <span><a href="{!! $response['url'] !!}" target="_blank">{!! $response['url'] !!}</a></span>
                                    </div>

                                    <hr>
                                    <div class="col-md-6">
                                        Content:
                                        <p>{{ $response['content'] }}</p>
                                    </div>
                                </div>
                            @endforeach -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
