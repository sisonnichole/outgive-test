@extends('layouts.app')

@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="card no-border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 header-text-padding">
                            <h5>My Profile</h5>
                            <hr>
                            <div class="card-actions">
                                <div class="row">
                                    <div class="col-sm-6">
                                        Name: <span>{{ $user->name }}</span>
                                    </div>
                                    <div class="col-sm-6">
                                        Email: <span>{{ $user->email }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        Website: 
                                        <span>
                                            @if (!$user->company->website)
                                                <span style="font-style: italic">None</span>
                                            @else
                                                {{ $user->company && $user->company->website ? $user->company->website : '' }}
                                            @endif
                                        </span>
                                    </div>
                                    <div class="col-sm-6">
                                        Logo: 
                                        <span>
                                            @if ($user->company && $user->company->logo_path)
                                                <img width=40% src="storage{{$user->company->logo_path}}" />
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
