@extends('layouts.app')
@section('content')
<div class="main-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="card no-border">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 header-text-padding">
                            <div class="col-sm-9 header-text-padding">
                                <h5>Actions</h5>
                                <div class="card-actions">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="page-actions text-right no-padding">
                                    <button style="float: right" type="button" class="btn btn-primary" data-toggle="modal" data-target="#addCompanyModal">Add New Company</button>
                                    <br>
                                </div>
                            </div>
                            <div class="card-body inner-2">
                                <table id="companies-datatable" name="companies-datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Password</th>
                                            <th>Website</th>
                                            <th>Logo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($companies as $company)
                                        <tr id="{{ $company->id }}">
                                            <td>{{ $company->company->name }}</td>
                                            <td>{{ $company->email }}</td>
                                            <td>{{ $company->password }}</td>
                                            <td>{{ $company->company->website }}</td>
                                            <td>
                                                @if ($company->company->logo_path)
                                                    <img width=30% src="storage{{$company->company->logo_path}}" />
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- Start Edit/Delete Actions -->
                            <div class="modal fade" id="companiesModal" name="companiesModal" role="dialog">
                                <div class="modal-dialog">
                                    <form action="{{route('company.update')}}" method = "POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="POST">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h6 class="modal-title">Edit/Delete Company</h6>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="row dataTable">
                                                    <div class="col-md-12">
                                                        <input type="hidden" class="form-control"  id="update_company_id" name="update_company_id">
                                                    </div>
                                                </div>
                                                <div class="row dataTable">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Name</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <input type="text" class="form-control" id="update_company_name" name="update_company_name" autofocus required>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row dataTable">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Email</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <input type="email" class="form-control" id="update_company_email" name="update_company_email" required>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row dataTable">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Password</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <input type="password" minlength="8" class="form-control" id="update_company_password" name="update_company_password" required>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row dataTable">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Website</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <input type="text" class="form-control"  id="update_company_website" name="update_company_website">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row dataTable">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Logo</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <input type="file" class="form-control-file" id="update_company_logo" name="update_company_logo">
                                                    </div>
                                                </div>
                                                <br>

                                            </div>
                                            <div class="modal-footer">
                                                <div class="col-sm-12 no-padding">
                                                    <button class="btn btn-danger delete" value="delete" name="action" type="submit">Delete</button>
                                                    <button style="float: right;" class="btn btn-primary edit" value="update" name="action" type="submit"><i class="icon-fa icon-fa-save">
                                                    </i>Save Changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- End Edit/Delete Actions -->


                            <div class="modal fade" id="addCompanyModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{route('company.store')}}" method = "POST" id="addCompanyForm" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="modal-header">
                                                <h4 class="modal-title">Add New Company</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row dataTable">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Company Name</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <input type="text" class="form-control" id="new_company_name" name="new_company_name" required>
                                                    </div>
                                                </div>
                                                <br>

                                                <div class="row dataTable">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Email</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <input type="email" class="form-control" id="new_company_email" name="new_company_email" required>
                                                    </div>
                                                </div>
                                                <br>

                                                <div class="row dataTable">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Password</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <input type="password" class="form-control" minlength="8" id="new_company_password" name="new_company_password" required>
                                                    </div>
                                                </div>
                                                <br>

                                                <div class="row dataTable">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Website</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <input type="text" class="form-control" id="new_company_website" name="new_company_website">
                                                    </div>
                                                </div>
                                                <br>

                                                <div class="row dataTable">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Logo</label>
                                                    </div>
                                                    <br>
                                                    <div class="col-md-12">
                                                        <input accept="image/*" type="file" class="form-control-file" id="new_company_logo" name="new_company_logo">
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-primary"><i class="icon-fa icon-fa-save"></i>Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
