<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Settings;
class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $table = "settings";
    public function run()
    {
        Settings::create([
            'key' => 'gen_email_for_registration',
            'value' => 'test@laravel.com',
        ]);
    }
}
