<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'superadmin@laravel.com',
            'name' => 'Super Admin',
            'password' => bcrypt('superadmin'),
            'user_type_id' => 1
        ]);

        User::create([
            'email' => 'user@laravel.com',
            'name' => 'User',
            'password' => bcrypt('user'),
            'user_type_id' => 2
        ]);
    }
}
