<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserType;

class UsersTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserType::create([
            'name' => 'Super Admin',
            'role' => 'superadmin',
        ]);
        UserType::create([
            'name' => 'User',
            'role' => 'user',
        ]);
    }
}
