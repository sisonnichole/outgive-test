<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Companies extends Model
{
    use HasFactory, Notifiable, SoftDeletes;

    protected $table = "companies";

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
