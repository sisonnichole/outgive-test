<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Settings;
use App\Models\UserType;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $userType = UserType::where('id', Auth::user()->user_type_id)->first();
        $settings = Settings::get();
        return view('settings', compact('userType', 'settings'));
    }
    
    public function update(Request $request)
    {
        $gen_email_for_registration = $request->input('gen_email_for_registration');
        Settings::where('key', 'gen_email_for_registration')->update(['value' => $gen_email_for_registration]);
        return redirect()->back();
    }
}