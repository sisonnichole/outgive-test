<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\UserType;
use App\Models\User;
use App\Models\Companies;
use App\Models\Settings;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userType = UserType::where('id', Auth::user()->user_type_id)->first();
        $companies = User::whereNull('deleted_at')->where('id', '<>', Auth::user()->id)->whereHas('company')->with('company')->orderBy('created_at', 'ASC')->get();
        // dd($users);
        return view('home', compact('userType', 'companies'));
    }

    public function newsletter()
    {
        $responses = Http::get('https://newsapi.org/v2/top-headlines?country=us&apiKey=9e56665dd12747d699700625112fe619')['articles'];
        $userType = UserType::where('id', Auth::user()->user_type_id)->first();
        return view('newsletter', compact('userType', 'responses'));
    }

    public function store(Request $request)
    {
        $new_company_name = $request->input('new_company_name');
        $new_company_email = $request->input('new_company_email');
        $new_company_password = bcrypt($request->input('new_company_password'));
        $new_company_website = $request->input('new_company_website');

        $exist = User::where('name', $new_company_name)->orWhere('email', $new_company_email)->whereNull('deleted_at')->first();
        if ($exist) {
            flash()->error('Company already exists!');
        } else {
            $user = new User();
            $user->name = $new_company_name;
            $user->email = $new_company_email;
            $user->password = $new_company_password;
            $user->user_type_id = 2;

            if ($user->save()) {
                $company = new Companies();
                $company->name = $new_company_name;
                $company->user_id = $user->id;
                $company->website = $new_company_website;
                if ($request->has('new_company_logo')) {
                    $image = $request->file('new_company_logo');
                    $path = Storage::disk('public')->putFileAs('companyFiles', $image, $image->getClientOriginalName());

                    $company->logo_path = '/'.$path;
                }
                $company->save();

                flash()->success('New company successfully saved!');
            } else {
                flash()->error('There was an error saving company!');
            }
        }

        return redirect()->back();
    }
    public function update(Request $request)
    {
        $update_company_id = $request->input('update_company_id');
        $update_company_name = $request->input('update_company_name');
        $update_company_email = $request->input('update_company_email');
        $update_company_password = bcrypt($request->input('update_company_password'));
        $update_company_website = $request->input('update_company_website');
        switch ($request->input('action')) {
        case 'update':
            $exist = User::where('name', $update_company_name)->orWhere('email', $update_company_email)->whereNull('deleted_at')->first();
            if ($exist && $exist->id != $update_company_id) {
                flash()->error('Company already exists!');
            } else {
                User::where('id', $update_company_id)->update(
                    [
                        'name' => $update_company_name,
                        'email' => $update_company_email,
                        'password' => bcrypt($update_company_password)
                    ]
                );

                Companies::where('user_id', $update_company_id)->update(
                    [
                        'name' => $update_company_name,
                        'website' => $update_company_website
                    ]
                );

                if ($request->has('update_company_logo')) {
                    $image = $request->file('update_company_logo');
                    $path = Storage::disk('public')->putFileAs('companyFiles', $image, $image->getClientOriginalName());

                    Companies::where('user_id', $update_company_id)->update(
                        [
                            'logo_path' => '/'.$path
                        ]
                    );
                }

                flash()->success('Company successfully updated!');

            }
            break;
        default:
            User::where('id', $update_company_id)->update(['deleted_at' => now()]);
            Companies::where('user_id', $update_company_id)->update(['deleted_at' => now()]);
            break;
        }
        return redirect()->back();
    }
    
}
