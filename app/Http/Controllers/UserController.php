<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use App\Models\UserType;

class UserController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
       //
    }

    public function show()
    {
        $userType = UserType::where('id', Auth::user()->user_type_id)->first();
        $user = User::where('id', Auth::user()->id)->with('company')->first();
        // dd($user);

        return view('user', compact('user', 'userType'));
        
    }
}
